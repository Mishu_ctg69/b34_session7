<div id="rounded">

    <img src="img/top_bg.gif" /><!-- image with rounded left and right top corners -->
    <div id="main" class="container"><!-- our main container element -->

        <h1>A simple AJAX driven jQuery website</h1> <!-- titles -->
        <h2>Because simpler is better</h2>

        <ul id="navigation"> <!-- the navigation menu -->
            <li><a href="#page1">Page 1</a></li> <!-- a few navigation buttons -->
            <li><a href="#page2">Page 2</a></li>
            <li><a href="#page3">Page 3</a></li>
            <li><a href="#page4">Page 4</a></li>
            <li><img id="loading" src="img/ajax_load.gif" alt="loading" /></li> <!-- rotating gif - hidden by default -->
        </ul>

        <div class="clear"></div> <!-- the above links are floated - we have to use the clearfix hack -->

        <div id="pageContent"> <!-- this is where our AJAX-ed content goes -->
            Hello, this is the default content
        </div>

    </div>

    <div class="clear"></div> <!-- clearing just in case -->

    <img src="img/bottom_bg.gif" /> <!-- the bottom two rounded corners of the page -->

</div>